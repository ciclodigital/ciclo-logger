# CicloLogger

Esta gem tem por objetivo ser uma interface de abstração com a ferramenta Ciclo Logger Central

## Instalando

Add this line to your application's Gemfile:

```ruby
gem 'ciclo_logger', git: 'https://adminciclodigital@bitbucket.org/ciclodigital/ciclo-logger.git'
```

And then execute:

```bash
$ bundle
```

## Utilizando

config/environments/production.rb

```ruby
  Rails.logger = CicloLogger.new(STDOUT)
```

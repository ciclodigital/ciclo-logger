require "ciclo_logger/version"

module CicloLogger
  class << self
    attr_accessor :configuration
  end

  def self.configure
    self.configuration ||= Configuration.new
    yield(configuration)
  end

  class Configuration
    attr_accessor :host
    attr_accessor :api_key
    attr_accessor :secret_key
    attr_accessor :levels

    def initialize
      @host       = 'logger.ciclo.digital'
      @api_key    = nil
      @secret_key = nil
      @levels     = [:error, :fatal]
    end
  end

  def self.config
    self.configuration ||= Configuration.new
  end
end
